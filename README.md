<div align="center">
Привет👋 Меня зовут Снежана!
</div>

---

<div align="center">
  
   Я Junior game-designer <img src="https://media.giphy.com/media/WUlplcMpOCEmTGBtBW/giphy.gif" width="30"> из России.
</div>

Интересуюсь видеоиграми с 9 лет. Сейчас занимаюсь изучением движка Unity и созданием нового проекта для пополнения портфолио,но хочу работать в компании в качестве гейм-дизайнера. Для этого я закончила курс “геймдизайнер” в образовательной онлайн-платформе "Нетология". Имеются документы об обучении, дизайн-документация и курсовой проект на движке UE4 с комментарием от учителя по данному проекту. Я намерена в дальнейшем совершенствовать свои навыки гейм-дизайнера, чтобы достигать наилучших результатов в своих работах.

<div align="center">
 <img src="https://raw.githubusercontent.com/FilimonovAlexey/FilimonovAlexey/50be29f8a24667802c3fa5393c879a2db3caf641/assets/github-snake.svg" width="700" height="200"/>
</div>

<div align="center">
 Cвязь со мной:
</div>

<div align="center">
  <a href="https://t.me/SnezhkaPie" target="_blank">
    <img src="https://img.shields.io/badge/Telegram-blue?style=for-the-badge&logo=Telegram&logoColor=white" alt="Telegram Badge"/>
  </a>
  <a href="https://vk.com/snezhopkatatarskaya" target="_blank">
   <img src="https://img.shields.io/badge/VK-blue?style=for-the-badge&logo=VK&logoColor=white" alt="VK Badge"/>
  </a>
  <a href="mailto:snezkaplaygames3@gmail.com" target="_blank">
   <img src="https://img.shields.io/badge/Gmail-red?style=for-the-badge&logo=Gmail&logoColor=white" alt="Gmail Badge"/>
</div>


---

Профессиональные практики:
- :four_leaf_clover: Системный дизайн
- :balance_scale: Балансировка игр
- :coffee: Дизайн-документация
- :books: Google Docs
- :star2: Google Sheets
- :space_invader: Blueprints
- :video_game: UE4
- :sparkles: GitLab, GitHub
- :art: SAI2
